import logging
import uuid
from datetime import datetime

import pandas as pd
from dataiku.customrecipe import *
from metadata_handler import MetadataHandler
from pandas import DataFrame

from dataiku_utils import check_recipe_config, get_input_dataset, get_output_dataset
from factr_helper import FactrHelper
from response_handler import ResponseHandler

logger = logging.getLogger(__name__)

"""retrieve and initialize variables from dataiku"""
input_dataset = get_input_dataset("input")
output_dataset = get_output_dataset("main_output")
recipe_config = get_recipe_config()
check_recipe_config(recipe_config)

output_dataset.write_schema_from_dataframe(pd.DataFrame(data={"init": [""]}))

delivery_metadata = {"delivery_date": datetime.now(), "id_delivery": str(uuid.uuid1())}


"""main dataset building:
1. Call serruR authentication api to get allowed entities and attributes
2. Call factR data api via the geocodR endpoint to retrive requested data via an address"""
with output_dataset.get_writer() as writer:
    factr = FactrHelper(recipe_config)
    for index, df in enumerate(input_dataset.iter_dataframes(chunksize=10000)):
        logger.info(f"starting chunk {index} from the input_dataset")

        df["api_call"] = df.apply(func=lambda row: factr.api_call(row), axis=1)
        df["api_response"] = df.apply(func=lambda row: factr.query_geocodR(row), axis=1)

        # flattening the response from the api into specific columns
        response_handler = ResponseHandler(delivery_metadata, recipe_config)
        df.apply(func=lambda row: response_handler.flatten(row), axis=1)
        output_dataframe = DataFrame(response_handler.flattened_results)

        if index == 0:
            output_dataset.write_schema_from_dataframe(
                output_dataframe[response_handler.columns_valid_dataframe()]
            )
        writer.write_dataframe(
            output_dataframe[response_handler.columns_valid_dataframe()]
        )


"""after main dataset is prepared we create an optional metadata dataset"""
try:
    metadata_output_dataset = get_output_dataset("metadata_output")
    if metadata_output_dataset is not None:
        metadata_output_dataset.write_schema_from_dataframe(
            pd.DataFrame(data={"init": [""]})
        )
        metadata_handler = MetadataHandler(delivery_metadata)
        metadata_handler.augment_metadata(
            factr.metadata, output_dataframe, delivery_metadata
        )
        # If no data is present no dataset is written
        if metadata_handler.metadata_dataframe is not None:
            metadata_output_dataset.write_with_schema(
                metadata_handler.metadata_dataframe[metadata_handler.valid_columns]
            )
except Exception as e:
    logger.critical(f"We had an error formating metadata: f{e}")

