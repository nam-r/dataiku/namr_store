from typing import Dict, List, Optional

from pandas.core.series import Series


class ResponseHandler:
    """Handles dict from api response in order to retrieve the appropriate self.column_names
        in a list of flattened dicts: selft.flattened_results
    """

    ColumnName = str
    ColumnValue = str
    flattened_results: List[Dict[ColumnName, ColumnValue]] = []
    column_names: Dict[ColumnName, str] = {}
    id_column: Optional[str]
    address_column: str

    def __init__(self, delivery_metadata: dict, recipe_config: Dict[str, str]):
        self.flattened_results = []
        self.output_columns = list(delivery_metadata.keys()) + [
            "id",
            "queried_address",
            "api_call",
            "api_call_status",
        ]
        self.delivery_metadata = delivery_metadata
        self.id_column = recipe_config.get("id_column", "id")
        self.address_column = recipe_config["address_column"]

    def flatten(self, row: Series) -> None:
        """Flatens api_responses in order to make the dicts answered by api flat dicts with just the key/values we want to display
            to transform that flat dict easily into a pandas.dataframe

            As a side effect of creating a list of flat dict for the final dataFrame, this function collects an ordered list of unique column names
            for all entities and attributes discovered in api_response

            Exception catching monitors api errors and returns data in a different dataframe schema (will be changed with v1)
        """

        api_response = row["api_response"]
        flattened_row = {
            "id": row.get(self.id_column, None),
            "queried_address": row[self.address_column],
            "api_call": row["api_call"],
            "api_call_status": api_response["api_call_status"],
        }
        if "data" in api_response:
            for feature in api_response["data"]["features"]:
                properties = feature["properties"]
                flattened_row["address"] = properties["label"]
                for entity_name, entity_data in feature["entity"][
                    "relationships"
                ].items():
                    if len(entity_data["data"]) == 0:  # case no relationship
                        continue
                    entity_data = entity_data["data"][0]["entity"]
                    entity_name_key = f"id_{entity_name}"
                    flattened_row[entity_name_key] = entity_data["id"]
                    self.column_names[entity_name_key] = ""
                    for attribute_name, attribute_data in entity_data[
                        "attributes"
                    ].items():
                        flattened_row.update(
                            self.get_flat_attribute_data(
                                entity_name, attribute_name, attribute_data
                            )
                        )
        self.flattened_results.append({**flattened_row, **self.delivery_metadata})

    def get_flat_attribute_data(
        self, entity_name: str, attribute_name: str, attribute_data: Dict
    ):
        """This is a helper function for self.flatten
        
            For every attribute name and data retrieved from the dict under features.entities.attributes
            we add corresponding values to the flattened_row dict. 
            These values are always decomposed into confidence, value and unit 
        """

        attribute_full_name = f"{entity_name}.{attribute_name}"
        flat_attribute_data = {}

        attribute_confidence_key = attribute_full_name + ".confidence"
        self.column_names[attribute_confidence_key] = ""
        flat_attribute_data[attribute_confidence_key] = attribute_data["confidence"]

        attribute_value_key = attribute_full_name + ".value"
        self.column_names[attribute_value_key] = ""
        flat_attribute_data[attribute_value_key] = attribute_data["value"]

        return flat_attribute_data

    def columns_valid_dataframe(self) -> List[ColumnName]:
        """uses previously harvested column_names and adds generic columns to the list of columns we want in our final dataframe 
        """

        return self.output_columns + list(self.column_names.keys())
