from typing import Dict, List

from pandas import DataFrame, to_numeric
from pandas.core.series import Series


class MetadataHandler:
    """Handles response from the metadata endpoint to format them for the metadata output dataset"""
    metadata_dataframe: DataFrame = None
    flattened_metadata: List[Dict] = []

    def __init__(self, delivery_metadata: Dict):
        self.valid_columns = list(delivery_metadata.keys()) + [
            "nature",
            "entity",
            "delivered_items",
            "name",
            "definition",
            "data_type",
            "unit",
            "version",
        ]

    def augment_metadata(
        self, factr_metadata: Dict, output_dataframe: Series, delivery_metadata: Dict
    ) -> None:
        """Gathers metadata from two sources and joines them in a dataframe:
            - delivery specific, timestamp and id_delivery as well as the number of delivered attributes
            - data specific, metadata for entities and attributes from factr

            As a side effect the metadata dataframe is created
        """

        delivered_attributes_count: DataFrame = output_dataframe[
            output_dataframe.columns.difference(
                [
                    "api_call",
                    "api_call_status",
                    "id",
                    "id_building",
                    "queried_address",
                    "address",
                ]
            )
        ].count(axis=0)
        for entity in factr_metadata.values():
            self.flattened_metadata.append(
                {**entity, **delivery_metadata, "nature": "entity"}
            )
            for attribute in entity["attributes"].values():
                try:
                    attribute["delivered_items"] = int(
                        delivered_attributes_count[f"{attribute['full_name']}.value"]
                    )
                except KeyError:
                    attribute["delivered_items"] = 0
                attribute["data_type"] = attribute.pop("type", "no data type")
                self.flattened_metadata.append(
                    {**attribute, **delivery_metadata, "nature": "attribute"}
                )
        if len(self.flattened_metadata) == 0:
            return
        self.metadata_dataframe = DataFrame(self.flattened_metadata)
        self.metadata_dataframe["delivered_items"] = to_numeric(
            self.metadata_dataframe["delivered_items"]
        )
