import logging
from typing import Dict, List, Optional

from pandas.core.series import Series
from requests import get

logger = logging.getLogger(__name__)

class FactrHelper:
    """ This class handles all interaction with the namr api

        Note, all apis are open and accessible, provided the api token is valid. Therefore there is no need for
        parameter escaping as calls could be made directly anyway.
    """

    geocodR_endpoint = "https://api.namr.it/v2/entity/address/geocodr"
    serruR_endpoint = "https://serrur.namr.it/v1/info"
    metadata_endpoint = "https://api.namr.it/v2/metadata/entity"
    allowed_attributes: List[str] = []
    allowed_includes: List[str] = []
    metadata: Dict = {}
    limit = 1
    recipe_config: Dict[str, str]

    def __init__(self, recipe_config: Dict[str, str]):
        self.recipe_config = recipe_config
        self.headers = {"Authorization": "Bearer " + recipe_config["api_token"]}
        self.get_scope()
        self.get_metadata()

    def query_geocodR(self, row: Series) -> Dict[str, str]:
        """Queries the geocodr using, the dataiku argument api_token
            returns a dict with the json the api answersed
        """

        geocodR_res = get(
            self.geocodR_endpoint,
            params={
                "q": row[f"{self.recipe_config['address_column']}"],
                "field": self.allowed_attributes,
                "include": self.allowed_includes,
                "relationship_field": "address.building.is_chosen.eq(true)",
                "limit": 1,
            },
            headers=self.headers,
        )
        if geocodR_res.status_code != 200:
            return {
                "api_call_status": (
                    str(geocodR_res.status_code)
                    + ", "
                    + geocodR_res.headers["Grpc-Message"]
                )
                if "Grpc-Message" in geocodR_res.headers
                else geocodR_res.status_code
            }
        return {**geocodR_res.json(), **{"api_call_status": geocodR_res.status_code}}

    def format_attributes_serruR_to_factR(self, attributes_by_entity) -> None:
        """ For all couples queries from serruR via the token, adds them to a list usable by factr 
            {
            example @arg attributes_by_entity = {
                "entity": "entity_name",
                "attributes": ["a", "b"]
            }
            via self.allowed_attributes and self.allowed_includes
        """
        # HORRIBLE HACK IRIS IS ONLY QUERYABLE IN FACTR V2 THROUGH BUILDING (NOT ADDRESS) --> removed from all plugin calls
        if attributes_by_entity["entity"] == "iris":
            return
        # END OF HACK
        elif attributes_by_entity["entity"] == "address":
            self.allowed_attributes.extend(attributes for attributes in attributes_by_entity["attributes"] if attributes != "*")
        else:
            self.allowed_attributes.extend(
                f"{attributes_by_entity['entity']}.{attribute}"
                for attribute in attributes_by_entity["attributes"]
            )
            self.allowed_includes.append(attributes_by_entity["entity"])

    def get_scope(self) -> None:
        """ This methods fetches the scope of entity and attributes the user can access based on provided token
            to prepare the query for the geocodR
        """

        serrur_res = get(self.serruR_endpoint, headers=self.headers)
        if serrur_res.status_code == 403:
            raise Exception(f"The token you provided is not valid for {self.serruR_endpoint}.")
        try:
            for attributes_by_entity in serrur_res.json()["user"]["attributes"]:
                self.format_attributes_serruR_to_factR(attributes_by_entity)
        except Exception as e:
            logger.warning(e)
            raise Exception(
                "We had an internal error, please try again. If this persists please contact us."
            )

    def get_metadata(self):
        """ Getting metadata for the metadata for each entity and attributes previously defined

            Address attributes are present in attribute_full_name withouth the 'entity.' prefix. 
            They are trivial or useless and ignored
        """

        try:
            for entity in self.allowed_includes:
                metadata_res = get(
                    self.metadata_endpoint + f"/{entity}", headers=self.headers
                )
                if metadata_res.status_code != 200:
                    raise Exception(
                        f"There was an error with our api:\n\n{metadata_res.__dict__}\n\n on :{self.metadata_endpoint + f'/{entity}'}\n"
                    )
                self.metadata[entity] = metadata_res.json()["entityMetadata"]
                self.metadata[entity].update({"attributes": {}, "name": entity})
            for attribute_full_name in self.allowed_attributes:
                (entity_name, attribute_name) = attribute_full_name.split(".")
                attribute_metadata_res = get(
                    self.metadata_endpoint + f"/{entity_name}/{attribute_name}",
                    headers=self.headers,
                )

                if attribute_metadata_res.status_code != 200:
                    if attribute_metadata_res.status_code == 400:
                        self.metadata[entity_name]["attributes"][attribute_name] = {
                            "definition": "no metadata for this attribute"
                        }
                    else:
                        raise Exception(
                            f"error from the api {attribute_metadata_res.__dict__}"
                        )
                else:
                    self.metadata[entity_name]["attributes"][
                        attribute_name
                    ] = attribute_metadata_res.json()["attributeMetadata"]
                self.metadata[entity_name]["attributes"][attribute_name].update(
                    {
                        "full_name": attribute_full_name,
                        "entity": entity_name,
                        "name": attribute_name,
                    }
                )
        except Exception as e:
            logger.warning(
                f"error querrying metadata: {e} \n\n metadata: {self.metadata}"
            )
            raise Exception(
                "We had an internal error, if this persists please contact us"
            )

    def api_call(self, row: Series) -> str:
        """ This the api query string to help with debugging in case api response is not 200 and someone wants to debug it directly
        """

        return (
            self.geocodR_endpoint
            + "?q="
            + row[f"{self.recipe_config['address_column']}"]
            + "&include="
            + str(self.allowed_includes)
            + "&field="
            + str(self.allowed_attributes)
            + "&relationship_field=address.building.is_chose.eq(true)"
        )
