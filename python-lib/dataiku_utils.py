from typing import Dict, Optional

import dataiku
from dataiku.customrecipe import get_input_names_for_role, get_output_names_for_role

"""Modules to retrieve datasets defined by dataiku user"""

# Retrieve array of dataset names from 'dataiku' roles, then create datasets
def get_input_dataset(title: str) -> dataiku.core.dataset.Dataset:
    input_names = get_input_names_for_role(title)
    input_datasets = [dataiku.Dataset(name) for name in input_names]
    return input_datasets[0]


def get_output_dataset(title: str) -> Optional[dataiku.core.dataset.Dataset]:
    output_names = get_output_names_for_role(title)
    output_datasets = [dataiku.Dataset(name) for name in output_names]
    try:
        return output_datasets[0]
    except:
        return None


def check_recipe_config(recipe_config: Dict[str, str]):
    """recipe config contains plugin parameters: api_token, address_column and option id_column
        mandatory nature of each parameter is not implemented by dataiku at time of check
        this is additional security
    """

    try:
        dummy = recipe_config["address_column"]
    except Exception as e:
        raise Exception(
            "Please enter an address column name in the plugin recipe inside dataiku"
        )
    try:
        dummy = recipe_config["api_token"]
    except Exception as e:
        raise Exception("Please enter an api token in the plugin recipe inside dataiku")
