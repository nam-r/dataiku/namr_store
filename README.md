![](./images/logo_namr.png)


## **namR store**  - Data enrichment plugin

Whether you are a business analyst or data engineer, this plugin recipe (DSS - [documentation](https://doc.dataiku.com/dss/latest/)), offers an easy access to the nam.R store API and data

In this README we are sharing all the needed informations to work with this plugin recipe :  

* Requirements 
* Deployment
* Access Token
* Getting started
* namR store
* Contributions

Need support? contact [sales@namr.com](mailto:sales@namr.com) 

## Requirements 

This plugin has been developped under DSS v7.0.2.  [documentation](https://doc.dataiku.com/dss/latest/release_notes/7.0.html)

- DSS v7.0.X or above
- Administrator right
- Access token to namR store `9E7jrcxBOT2aoGSVcyIdGDzMzhasyhihw7qg2Dx7nc1UhssmuRPtsoWWPfWRqZZLYeAB+tyqOdMtEUrdamwnGg==`
- Dataset test [download here](./data/plugin_demo_address.csv)


## Deployment

*This plugin recipe will be available soon in the dataiku [plugin store](https://doc.dataiku.com/dss/latest/plugins/installing.html#installing-from-the-store)*

DSS provides two ways of installing a plugin recipe

* Using python API 

```python
import dataiku
client = dataiku.api_client()

repo_url = "git@gitlab.com:nam-r/dataiku/namr_store.git"
future = client.install_plugin_from_git(repo_url, checkout = 'master')
future.wait_for_result()
```

* Using UI 

Go to `[DSS_URL]/plugins-explore/store/`

In the top right corner of your DSS click on  `ADD PLUGIN` button

![](./images/add_plugin.png)

You can install a new plugin: 

* from a zip file [documentation](https://doc.dataiku.com/dss/latest/plugins/installing.html#installing-from-a-zip-file) 
* from a git repository [documentation](https://doc.dataiku.com/dss/latest/plugins/installing.html#from-a-git-repository)

Make sur the plugin is available under `[DSS_URL]/plugins-explore/installed/`

## Access token

Our sales team will be glad to get you an acces token to namR store (trial ou premium) [sales@namr.com](mailto:sales@namr.com) 

In the context of namR store, a token might have three scopes restrictions:

* Geographical scope,  eg. one municipality - Clichy insee code 92024
* Data scope: data related to a specific entity, eg. to building characteristics: building construction period, building height
* Limitations and quotas

**Black Spots**
  - At the moment iris cannot be queried through the plugin and only from namr's Api V2.

## Getting started

### 1. Flow 

At recipe creation, you will be ask to provide 3 datasets: 

* 1 input dataset 

* 2 output datasets

![](./images/flow.png)

  

  Before running the JOB, ACCESS TOKEN must be filled in the settings.


![](./images/token.png)


### 2. Datasets description

#### input dataset

This part describes input dataset provided by users.

It  requires at least two fields : 

| Field name | Field type |               Definition                |           Comment           |
| :--------: | :--------: | :-------------------------------------: | :-------------------------: |
|     id     |            |        unique identifier per row        | no restriction on data type |
|  address   |   String   | address geolocalized field for each row |     one address per row     |

#### output datasets

This part describes the datasets created by this plugin recipe

- Data

  This dataset provides all fields available for each row of the input dataset. It, also, gives  extra informations an API calls (query and status) for each row processed.

  |   Field name    | Field type |                          Definition                          |
  | :-------------: | :--------: | :----------------------------------------------------------: |
  |  delivery_date  |    date    |                    DSS job starting date                     |
  |   id_delivery   |    int     |              unique identifier of the delivery               |
  |       id        |            |             field ` id`provided in input dataset             |
  | queried_address |    text    |         field ` address ` provided in input dataset          |
  |    api_call     |    text    |                query sent to namR store API                 |
  | api_call_status |    Int     | status code != 200 meaning row has not been properly processed |
  |     Field**     |            |        all fields available under token access scope         |

- Metadata

  This dataset provides meta informations on delivered fields. 

  |   Field name    | Field type |                         Definition                          |
  | :-------------: | :--------: | :---------------------------------------------------------: |
  |  delivery_date  |    date    |                    DSS job starting date                    |
  |   id_delivery   |    int     |              unique identifier of the delivery              |
  |     nature      |   string   | values:  `entity`  / `attribute` nature of fields provided  |
  |     entity      |   string   |                   entity name `building`                    |
  | delivered_items |    Int     |     number of non-null values delivered for each field      |
  |      name       |   String   |                  Field name (eg.`height`)                   |
  |   definition    |   String   | Field meaning (e.g. `building height is the difference ...` )|
  |    data_type    |   String   |                 Field data type (e.g.`real`)                 |
  |      unit       |   String   |                        unit (e.g. `m`)                       |
  |     Version     |    Int     |              field delivered version (e.g. `4`)             |

## namR store

Each row found in the input dataset are processed by the namR store engine with the following pipelines : 

***Step1***: Geocoding 

Geocoding is the process of geolocalized a text-based described of a location (address or place name).

For each row, our in-house geocoder will geolocalized your address and provide an internal unique identifier.  

Null values are returned for addresses out of the token geographical scope. (see above)

***Step2***: Link between addresses and entities

Then each address is linked to the requested entities (eg. building). 

***Step3***: Fetch informations

Token data scope are returned for each entity identified. 

eg. building -> building height, building construction period ... 

***Step4***: Return information 

Each information available on the data scope will be returned on a denormalized way.


The above description provide a very simple overview of the namR store engine. 

## Contributions

This plugin recipe is an open source project and might not cover all features needed for your project. 
You are more than welcome if you want to contribute this project. 

If you have any suggestion about new features we could implement, do not hesitate to share it with our team [sales@namr.com](mailto:sales@namr.com) 

### 1. Process

* Fork this repository 
* create your own branch `git checkout -b <branch_name>` (one branch per new feature)
* push your modifications to our git repository  `git push origin <project_name>/<location>`
* submit your merge request (do not forget to update the documentation ;) )
* our development team will come back to you after a quick code review

### 2. Setup development environment: 

1. Create a new plugin in dev mode
2. Load the branch you created: by clicking on the git logo you can select a branch, select your mr - WARNING if two persons are working in sandbox plugin they will encounter problems you should create a second sandbox plugin and set it up to use your branch if that is the case because a plugin can only be linked with a branch at a given time
3. Go to a test or sandbox env and go all out!
4. Once you are satisfied with what you have submit your mr. To do so, push the dataiku local commits to the repo in the plugin page.

### 3. Merging

1. Once your mr is merged into master, you must update your target branch (dev / prod)
2. Go to the dev / prod plugin
3. Select pull from the arrows next to the git logo
4. Your target plugin is up to date with master and therefore with your previously merged mr
### 4. Issues

If you are facing any issues while installing or working with the plugin recipe do not hesitate to open git issues, our contact our us at [ADD CONTACT]

## Tests

Tests are made via the makefile.

Integration tests run @ namR's dataiku instance via a config file templated `tests/python/resources/dss_access_config_template.json`. 

To run the tests:
1. create a version of this template as `tests/python/resources/dss_access_config.json`
2. `make integration-tests`
